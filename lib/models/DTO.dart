class DTO {
  final String id;
  final String image;
  final int likes;

  DTO({this.id, this.image, this.likes});
}
