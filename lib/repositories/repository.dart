import 'package:flutter/material.dart';
import 'package:flutter_lecture_7/models/DTO.dart';
import 'package:flutter_lecture_7/services/photo_service.dart';

class Repository extends StatelessWidget {
  var list;
  Future<List<DTO>> getListImage() async {
    return list = await PhotoService.getData().then(
      (value) {
        return value
            .map(
              (e) => DTO(
                id: e['id'],
                image: e['urls']['raw'],
                likes: e['likes'],
              ),
            )
            .toList();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    getListImage();
    return FutureBuilder(
      future: getListImage(),
      builder: (context, AsyncSnapshot<List<DTO>> snapshot) {
        if (snapshot.hasData) {
          return Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.75,
            child: SingleChildScrollView(
              child: Column(
                children: snapshot.data
                    .map(
                      (e) => Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('ID: ${e.id}'),
                          Padding(
                            padding: EdgeInsets.all(10),
                            child: SizedBox(
                              width: double.infinity,
                              height: 150,
                              child: FittedBox(
                                fit: BoxFit.contain,
                                child: Image.network(e.image),
                              ),
                            ),
                          ),
                          Text('LIKES: ${e.likes}'),
                          Divider(
                            color: Colors.black,
                            indent: 0,
                            endIndent: 0,
                          )
                        ],
                      ),
                    )
                    .toList(),
              ),
            ),
          );
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }
}
